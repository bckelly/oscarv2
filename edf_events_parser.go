package main

import (
	"bytes"
	"fmt"
	"os"
	"strconv"
)

var ANNOTATION_SEPARATOR = []uint8{20}
var ANNOTATION_DURATION = []uint8{21}
var ANNOTATION_END = []uint8{0}

// You could also refer to annotations as "Events"
type annotation struct {
	offset   int
	duration float64
	text     string
}

/*
 * Main function for parsing an annotation section of an EDF file.
 */
func parseAnnotation(byteReader bytes.Reader, byteIndex *int64, numberOfSamples int) annotation {
	newAnnotation := annotation{}
	annotationStart := byteIndex // starting point of the annotation
	annotationEnd := *annotationStart + (int64(numberOfSamples) * 2)

	// This loop declaration is probably redundant?
	for *byteIndex < annotationEnd {
		offset := collectOffset(byteReader, byteIndex)
		duration := collectDuration(byteReader, byteIndex)
		annotationText := collectAnnotationText(byteReader, byteIndex)

		newAnnotation = annotation{
			offset:   offset,
			duration: duration,
			text:     annotationText,
		}

		// Now that we collected the annotation, move to the end of the annotation block.
		character := extractByteArray(byteReader, 1, byteIndex)
		for bytes.Compare(character, ANNOTATION_END) == 0 {
			// Just cycle until the end of the signal, we already have the annotation data
			character = extractByteArray(byteReader, 1, byteIndex)
		}
		// After cycling to the end, the next character was not an end character, so pull the index back by one.
		//*byteIndex = *byteIndex - 1
		if *byteIndex >= annotationEnd {
			break
		}
	}

	return newAnnotation
}

// There are sometimes a handful of separator characters between sections, skip them and set the index to the next
//
//	non-separator character.
func skipSeparatorBlock(byteReader bytes.Reader, byteIndex *int64) {
	character := extractByteArray(byteReader, 1, byteIndex)
	for bytes.Compare(character, ANNOTATION_SEPARATOR) == 0 {
		character = extractByteArray(byteReader, 1, byteIndex)
	}
	// since we found a non-separator character, set the index back one so the next extract pulls a non-separator character
	//*byteIndex--
}

// After the duration, skip forward until you hit a separator byte
func findNextSeparator(byteReader bytes.Reader, byteIndex *int64) {
	character := extractByteArray(byteReader, 1, byteIndex)
	for bytes.Compare(character, ANNOTATION_SEPARATOR) != 0 {
		character = extractByteArray(byteReader, 1, byteIndex)
	}
}

func isSign(sign string) bool {
	return sign == "+" || sign == "-"
}

// Annotation must start with a +/- sign
func checkSignValue(sign string) bool {
	if sign == "+" {
		return true
	} else if sign == "-" {
		return false
	} else {
		ErrorLogger.Println(fmt.Sprintf("Sign value was not +/- character!  sign: %s", sign))
		os.Exit(-1)
	}
	// THIS SHOULD NEVER BE HIT
	return false
}

func collectOffset(byteReader bytes.Reader, byteIndex *int64) int {
	text := ""

	character := extractByteArray(byteReader, 1, byteIndex)
	signValue := checkSignValue(string(character))

	character = extractByteArray(byteReader, 1, byteIndex)
	// collect the offset
	for bytes.Compare(character, ANNOTATION_SEPARATOR) != 0 {
		text += string(character)
		*byteIndex++
		character = extractByteArray(byteReader, 1, byteIndex)
	}

	offset, errors := strconv.Atoi(text)
	if errors != nil {
		// TODO: This could be a bigger problem, not sure yet
		DebugLogger.Println(fmt.Sprintf("Faulty offset in annotation record. %s", errors.Error()))
	}

	// If we got a negative value in the sign, invert the offset to negative
	if !signValue {
		offset = -offset
	}

	return offset
}

func collectDuration(byteReader bytes.Reader, byteIndex *int64) float64 {
	text := ""
	character := extractByteArray(byteReader, 1, byteIndex)
	sign := true

	// collect the duration
	for bytes.Compare(character, ANNOTATION_DURATION) != 0 {
		characterString := string(character)
		if isSign(characterString) { // If we got a +/- character, don't add it to the string, save the sign value and invert it at the end
			sign = checkSignValue(characterString)
		} else {
			text += characterString
		}
		character = extractByteArray(byteReader, 1, byteIndex)
	}

	// TODO: Need to handle this error case. Go is being awful about setting the error variable
	duration, _ := strconv.ParseFloat(text, 32)
	if !sign { // If we got a negative duration, flip the value. Should never happen.
		duration = -duration
	}

	return duration
}

func collectAnnotationText(byteReader bytes.Reader, byteIndex *int64) string {
	annotationText := ""
	findNextSeparator(byteReader, byteIndex) // Small cleanup, just move the pointer out of the separators between sections

	character := extractByteArray(byteReader, 1, byteIndex)
	for bytes.Compare(character, ANNOTATION_END) != 0 {
		//character := extractByteArray(byteReader, 1, byteIndex)

		// I don't think this should ever happen... you should have a separator after the annotation text before the end
		//    ... Unless there's an unknown 4th item in the annotation ...
		if bytes.Compare(character, ANNOTATION_SEPARATOR) == 0 {
			character = extractByteArray(byteReader, 1, byteIndex)
			continue
		}

		// collect the annotation text
		// officially UTF-8 is allowed here, so don't mangle it
		// The original grabbed the block of text after it had already traversed it, not really sure why. It
		// Kept an index at the start of the text block and when it found the end it grabbed everything in between
		// and converted it to a string. I'm just building the string as I go instead.
		annotationText = annotationText + string(character)
		character = extractByteArray(byteReader, 1, byteIndex)
	}

	return annotationText
}
