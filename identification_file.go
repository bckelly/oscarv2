package main

import (
	"fmt"
	"path/filepath"
	"strings"
)

// Keys in the Identification.tgt file
// The file typically has this kind of format:
//
// #IMF 0001
//
// #VIR 0065
//
// #RIR 0064
//
// #PVR 0065
//
// #PVD 001A
// ... etc
const (
	IMF string = "IMF"
	VIR        = "VIR"
	RIR        = "RIR"
	PVR        = "PVR"
	PVD        = "PVD"
	CID        = "CID"
	RID        = "RID"
	VID        = "VID"
	SRN        = "SRN"
	SID        = "SID"
	PNA        = "PNA"
	PCD        = "PCD"
	PCB        = "PCB"
	MID        = "MID"
	FGT        = "FGT"
	BID        = "BID"
)

type Identification struct {
	IMF string
	VIR string
	RIR string
	PVR string
	PVD string
	CID string
	RID string
	VID string // Found in the STR.edf file header, unknown meaning
	SRN string // Serial Number
	SID string
	PNA string // Product Name AKA "Series",Just replace underscores
	PCD string // Product Code AKA Model Number TODO: if the model number > 39000 the device is untested and we need to ask for a copy of the SD card
	PCB string // Printed Circuit Board Number
	MID string // Found in the STR.edf file header, unknown meaning
	FGT string
	BID string
}

// Figure out and clean up the product name
func parseProductName(inputName string) string {
	// We expect the value to be one of:
	//   AirSense 11
	//   AirSense 10
	//   AirCurve 11
	//   AirCurve 10 -- I don't think this exists?
	//   S9

	inputName = strings.ReplaceAll(inputName, "_", " ")
	inputName = strings.ReplaceAll(inputName, "(", "")
	inputName = strings.ReplaceAll(inputName, ")", "")

	// If it is not AirCurve or AirSense, it is a Series 9
	if !strings.Contains(inputName, "AirSense") && !strings.Contains(inputName, "AirCurve") {
		// In OSCAR we assumed it was an S9, so prepend with that
		inputName = "S9 " + inputName
	}
	return inputName
}

// Given the file path of the Identification.tgt file, parse out an Identification struct
func parseIdentificationFile(dataDirectory string) Identification {
	// TODO: Apparently AirSense 11 has a json file for its Identification file
	// TODO: The old code would parse both files if they existed in the file structure. We shouldn't do that.
	filename := filepath.Join(dataDirectory, IDENTIFICATION_FILE)
	if !checkFileExists(filename) {
		ErrorLogger.Println(fmt.Sprintf("Cannot find the Identification file at %s", filename))
	}

	fileContents := readCleanedFile(filename)

	// TODO: This looks awful, I hope there's a cleaner way
	IMFVariable := ""
	VIRVariable := ""
	RIRVariable := ""
	PVRVariable := ""
	PVDVariable := ""
	CIDVariable := ""
	RIDVariable := ""
	VIDVariable := ""
	SRNVariable := ""
	SIDVariable := ""
	PNAVariable := ""
	PCDVariable := ""
	PCBVariable := ""
	MIDVariable := ""
	FGTVariable := ""
	BIDVariable := ""

	for _, line := range fileContents {
		line = strings.ReplaceAll(line, "#", "")
		parts := strings.Split(line, " ")
		key := parts[0]
		value := parts[1]

		switch key {
		case IMF:
			IMFVariable = value
		case VIR:
			VIRVariable = value
		case RIR:
			RIRVariable = value
		case PVR:
			PVRVariable = value
		case PVD:
			PVDVariable = value
		case CID:
			CIDVariable = value
		case RID:
			RIDVariable = value
		case VID:
			VIDVariable = value
		case SRN:
			SRNVariable = value
		case SID:
			SIDVariable = value
		case PNA:
			PNAVariable = parseProductName(value)
		case PCD:
			PCDVariable = value
		case PCB:
			PCBVariable = value
		case MID:
			MIDVariable = value
		case FGT:
			FGTVariable = value
		case BID:
			BIDVariable = value
		default:
			WarningLogger.Println(fmt.Sprintf("Unknown column type found in identification file: %s, value: %s, full line: %s", key, value, line))
		}
	}

	id := Identification{
		IMFVariable,
		VIRVariable,
		RIRVariable,
		PVRVariable,
		PVDVariable,
		CIDVariable,
		RIDVariable,
		VIDVariable,
		SRNVariable,
		SIDVariable,
		PNAVariable,
		PCDVariable,
		PCBVariable,
		MIDVariable,
		FGTVariable,
		BIDVariable,
	}

	return id
}
