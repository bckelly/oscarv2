package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// These are intended to be used throughout the project to access the logging functionality
var (
	DebugLogger   *log.Logger
	InfoLogger    *log.Logger
	WarningLogger *log.Logger
	ErrorLogger   *log.Logger
)

// Create the loggers available to the project.
// Debug: More logging than usual, useful for finding values of variables as we step through the code.
// Info: General logging, helpful information, user readable if necessary
// Warning: There could be a problem but it isn't causing instability just yet.
// Error: There's an actual problem. Describe what we know about the problem so someone has a chance to fix it.
func initializeLoggers() {
	os.Remove("OSCAR.log") // TODO: This is only for testing
	file, err := os.OpenFile("OSCAR.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	DebugLogger = log.New(file, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	InfoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// Important File and Folder names located in the ResMed SD Card.
const (
	DATALOG_FOLDER         string = "DATALOG"
	IDENTIFICATION_FILE           = "Identification.tgt"
	PRIMARY_STR_EDF_FILE          = "STR.edf"
	SECONDARY_STR_EDF_FILE        = "STR.edf.gz"
)

func main() {
	initializeLoggers()

	sleepDataDirectory := "/home/brian/Documents/sleep_data"
	//sleepDataDirectory := "C:\\Users\\Brian\\Documents\\sleep_data"

	id := parseIdentificationFile(sleepDataDirectory)
	parseStrEdfFile(filepath.Join(sleepDataDirectory, PRIMARY_STR_EDF_FILE))
	InfoLogger.Println("parsed Product Name is: " + id.PNA)

	allFiles := findAllFilesInDirectory(filepath.Join(sleepDataDirectory, DATALOG_FOLDER))

	// Remove CRC (checksum) files because we aren't going to use them
	edfFiles := []string{}
	for _, file := range allFiles {
		if !strings.Contains(file, ".crc") {
			if readFileLength(file) > 0 {
				edfFiles = append(edfFiles, file)
				parseEdfFile(file)
				println(file)
			} else {
				DebugLogger.Println(fmt.Sprintf("Found a zero length file: %s", file))
			}
		}
	}

	// TODO: I can't parse event files.
	//testEdfAnnotationsFile := "DATALOG/20211101/20211101_224150_EVE.edf"
	//fullFilePath := filepath.Join(sleepDataDirectory, testEdfAnnotationsFile)
	//parseEdfFile(fullFilePath)
	//InfoLogger.Println(strEdf)
	return
}
