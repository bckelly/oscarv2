package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Verify whether a given filename exists in the filesystem.
func checkFileExists(filename string) bool {
	if _, err := os.Stat(filename); err == nil {
		return true
	} else {
		return false
	}
}

// More basic than read as string. Given a filename, extract the bytes of the file.
// Used for EDF parsing and casting to string.
func readFileBytes(filename string) []byte {
	contents, _ := os.ReadFile(filename)

	return contents
}

// Given a filename (assumed to be the full path to the file), read the contents, cast them to a string, and return them
func readFileAsString(filename string) string {
	contents := readFileBytes(filename)

	return string(contents)
}

// Given a filename as input, read the file and extract the content line by line.
// If the line contains only whitespace, ignore it.
func readCleanedFile(filename string) []string {
	contents := readFileAsString(filename)

	lines := strings.Split(contents, "\n")

	trimmedLines := []string{}
	for _, line := range lines {
		if len(strings.TrimSpace(line)) > 0 {
			trimmedLines = append(trimmedLines, line)
		}
	}
	return trimmedLines
}

// Given a directory as an input, recursively walk the file tree and list all files found within the directory
func findAllFilesInDirectory(directory string) []string {
	files := []string{}
	err := filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			ErrorLogger.Println(err)
			return nil
		}

		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	DebugLogger.Println(fmt.Sprintf("Searched directory %s and found %d files", directory, len(files)))
	return files
}

func readFileLength(filename string) int {
	file, _ := os.Stat(filename)
	return int(file.Size())
}
