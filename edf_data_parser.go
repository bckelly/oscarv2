package main

import (
	"bytes"
	"encoding/binary"
	"strconv"
	"strings"
)

func parseEdfDataFile(byteReader bytes.Reader, byteIndex *int64, parsedEdfHeaders edfHeaders) []signal {
	edfSignalHeaders := parsedEdfHeaders.signalHeaders
	edfStandardFields := parsedEdfHeaders.standardHeaders
	numberOfSignals := edfStandardFields["numberOfSignals"]

	numberOfSignalsInt, _ := strconv.Atoi(numberOfSignals)

	signalTypes := make([]signal, numberOfSignalsInt)

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].label = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "label", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].transducer = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "transducer", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].physicalDimension = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "physicalDimension", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].physicalMinimum = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "physicalMinimum", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].physicalMaximum = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "physicalMaximum", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].digitalMinimum = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "digitalMinimum", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].digitalMaximum = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "digitalMaximum", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].prefiltering = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "prefiltering", i)
	}

	for i := 0; i < numberOfSignalsInt; i++ {
		signalTypes[i].samplesPerRecord = findEdfSignalHeaderValueOrEmpty(edfSignalHeaders, "samplesPerRecord", i)
	}

	// Begin parsing data records!

	// For every data record we know should be there,
	//   for every type of Signal in the data record,
	//     for how many samples we know should be in the Signal...
	numberOfRecords, _ := strconv.Atoi(edfStandardFields["numberOfRecords"])
	//annotationArray := []annotation{}
	for i := 0; i < numberOfRecords; i++ {
		for j := 0; j < len(signalTypes); j++ {
			signalItem := signalTypes[j]
			if strings.Contains(signalItem.label, "Annotations") {
				// TODO: I can't get this to work. I can get it to parse annotations OR 16-bit data, but not both in
				//   The same file. I don't understand.
				//numberOfSamples, _ := strconv.Atoi(signalItem.samplesPerRecord)
				//newAnnotation := parseAnnotation(byteReader, byteIndex, numberOfSamples)
				//annotationArray = append(annotationArray, newAnnotation)
			} else {
				sampleCount, _ := strconv.Atoi(signalItem.samplesPerRecord)
				for k := 0; k < sampleCount; k++ {
					byteArray := extractByteArray(byteReader, 2, byteIndex)
					u := binary.LittleEndian.Uint16(byteArray)
					sampleItem := sample{}
					sampleItem.measurement = int(u)
					signalTypes[j].samplesArray = append(signalTypes[j].samplesArray, sampleItem)
					// Read 16 bytes and cast as a 16-bit integer
					//   pass the 16-bit integer into the sample item's data array as a single measurement
				}
			}
		}
		print()
	}

	return signalTypes
}
