package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

// The various EDF files will typically look like:
// 20221106_100000_CSL.edf
// 20221106_100000_BRP.edf
const (
	EDF_EVE string = "EVE" // Event annotations, Central Apnea, Apnea, Hypopnea, etc
	EDF_BRP string = "BRP" // High resolution graph data
	EDF_PLD string = "PLD" // Low resolution graph data
	EDF_SAD string = "SAD" // Pulse oximetry data
	EDF_CSL string = "CSL" // Cheynes-Stokes Respiration annotations
	EDF_AEV string = "AEV"
	EDF_CRC string = "CRC" // Checksum file. Crc16 is mentioned in the annotations, maybe related?
)

// A Signal is a Type of measurement. Such as breaths-per-minute or Tidal Volume Pressure
type signal struct {
	label             string
	transducer        string
	physicalDimension string
	physicalMinimum   string
	physicalMaximum   string
	digitalMinimum    string
	digitalMaximum    string
	prefiltering      string
	samplesPerRecord  string
	reserved          string
	samplesArray      []sample
}

type sample struct {
	measurement int
}

type edfHeaders struct {
	standardHeaders map[string]string
	signalHeaders   map[string][]string
}

// Bananas crazy things happen in here. All of it is based on the European Data Format specification:
//
//	https://www.edfplus.info/specs/edf.html
//
// Basically, parse out useful information from the EDF(+) format
func parseEdfFile(filename string) []signal {
	fileBytes := readFileBytes(filename)
	fileLength := len(fileBytes)
	// TODO: This isn't the correct way to check the length of an EDF file header
	headerLength := strings.Index(string(fileBytes), "\n")
	if fileLength == 0 {
		ErrorLogger.Println("Invalid EDF format. File is empty.")
	}
	// Minimum header length for EDF files is 256 for standard EDF
	// TODO: Calculate this correctly. Newline is not the right delimiter. Header should be <= 256 chars
	DebugLogger.Println(fmt.Sprintf("Header length: %d", headerLength))

	byteReader := bytes.NewReader(fileBytes)
	byteIndex := int64(0)

	parsedEdfHeaders := parseEdfHeaders(*byteReader, &byteIndex)
	// The Annotations file is a little different from the rest. Instead of data measurements for cmH2O or pulse
	//   Oximetry, it records the Apnea-related events. As such it is more text based than the data files. The iteration
	//   through the event list is quite different from data traversal.
	if strings.Contains(filename, EDF_EVE) {
		DebugLogger.Println(fmt.Sprintf("Found an event file: %s", filename))
		DebugLogger.Println(fmt.Sprintf("Currently I am unable to parse event files.")) // TODO: Fix the ability to parse event files.
	}
	return parseEdfDataFile(*byteReader, &byteIndex, parsedEdfHeaders)
}

// Find the signal value header named label at index if it exists, or else return nothing.
func findEdfSignalHeaderValueOrEmpty(edfSignalHeaders map[string][]string, label string, index int) string {
	if index < len(edfSignalHeaders[label]) && len(edfSignalHeaders[label]) > 0 {
		return edfSignalHeaders[label][index]
	}
	return ""
}

// Given a byte array, take a section of size `blockSize` starting at index `offset`
func extractByteArray(byteReader bytes.Reader, blockSize int, offset *int64) []byte {
	byteStorage := make([]byte, blockSize)
	_, _ = byteReader.ReadAt(byteStorage, *offset)
	*offset = *offset + int64(blockSize)

	return byteStorage
}

// Given a byte array, take a section of size `blockSize` starting at index `offset` and convert it to string.
func extractByteArrayString(byteReader bytes.Reader, blockSize int, offset *int64) string {
	byteStorage := extractByteArray(byteReader, blockSize, offset)

	output := string(byteStorage)
	output = strings.TrimSpace(output)
	return output
}

// The first part of the EDF header will be the standard fields in the first 256 bytes
func extractEdfStandardFields(byteReader bytes.Reader, byteIndex *int64) map[string]string {
	edfStandardMap := make(map[string]string)

	edfStandardMap["version"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["patientId"] = extractByteArrayString(byteReader, 80, byteIndex)
	edfStandardMap["recordingId"] = extractByteArrayString(byteReader, 80, byteIndex)
	edfStandardMap["startDate"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["startTime"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["headerBytes"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["reserved"] = extractByteArrayString(byteReader, 44, byteIndex)
	edfStandardMap["numberOfRecords"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["durationOfRecord"] = extractByteArrayString(byteReader, 8, byteIndex)
	edfStandardMap["numberOfSignals"] = extractByteArrayString(byteReader, 4, byteIndex)

	return edfStandardMap
}

// For verification, print out the standard EDF header values.
// Expected format is:
// ["version": "0", "patientId": "abcdef" ...etc ]
func printEdfStandardFields(edfStandardFields map[string]string) {
	DebugLogger.Println("Standard EDF fields:")
	for key, value := range edfStandardFields {
		DebugLogger.Println("  " + key + fmt.Sprintf(" [%s]", value))
	}
}

// Each EDF Plus Section follows a similar pattern to the EDF fields but each section is sized based on the numberOfSamples.
// Mostly the same style as the EDF header section, but each EDF plus section is an array of values
func extractEDFSignalHeader(numberOfSignals int, sectionSize int, byteReader bytes.Reader, byteIndex *int64) []string {
	parsedFields := []string{}
	for i := 0; i < numberOfSignals; i++ {
		signalHeaderString := extractByteArrayString(byteReader, sectionSize, byteIndex)
		parsedFields = append(parsedFields, signalHeaderString)
	}
	return parsedFields
}

// At the end of the 256 byte primary EDF section we have the EDF Plus section. The size of the EDF+ section is variable
// With the number of signals and the contents are arbitrary. We'll do our best to parse them.
func extractAllEdfSignalHeaders(numberOfSignalsField string, byteReader bytes.Reader, byteIndex *int64) map[string][]string {
	// EDF+ header fields
	numberOfSignals, _ := strconv.Atoi(numberOfSignalsField)
	edfSignalHeaders := make(map[string][]string)

	edfSignalHeaders["label"] = extractEDFSignalHeader(numberOfSignals, 16, byteReader, byteIndex)
	edfSignalHeaders["transducer"] = extractEDFSignalHeader(numberOfSignals, 80, byteReader, byteIndex)
	edfSignalHeaders["physicalDimension"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["physicalMinimum"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["physicalMaximum"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["digitalMinimum"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["digitalMaximum"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["prefiltering"] = extractEDFSignalHeader(numberOfSignals, 80, byteReader, byteIndex)
	edfSignalHeaders["samplesPerRecord"] = extractEDFSignalHeader(numberOfSignals, 8, byteReader, byteIndex)
	edfSignalHeaders["reserved"] = extractEDFSignalHeader(numberOfSignals, 32, byteReader, byteIndex)

	return edfSignalHeaders
}

// For verification, print out the additional EDF+ header field values.
// Expected format is:
// ["labels": [Date, MaskOn, MaskOff... etc]... ]
func printEdfSignalHeaders(edfPlusHeaderFields map[string][]string) {
	DebugLogger.Println("EDF Signal Header Fields:")
	for key, value := range edfPlusHeaderFields {
		DebugLogger.Println("  " + key + fmt.Sprintf(" %s", value))
	}
}

// Possibly redundant function call. If I leave it as a call to the EDF function, just replace the call to this function
// with a call to the EDF file parser.
func parseStrEdfFile(filename string) []signal {
	// TODO: This might be unnecessary, I'm not sure if there's something special about the STR.edf file or not ... yet
	// TODO: There is apparently a situation where a STR.edf.gz might exist so we should extract that first and then parse it.
	return parseEdfFile(filename)
}

func parseEdfHeaders(byteReader bytes.Reader, byteIndex *int64) edfHeaders {
	// Standard EDF fields
	edfStandardFields := extractEdfStandardFields(byteReader, byteIndex)
	numberOfSignals := edfStandardFields["numberOfSignals"]
	printEdfStandardFields(edfStandardFields)

	// EDF Signal headers
	edfSignalHeaders := extractAllEdfSignalHeaders(numberOfSignals, byteReader, byteIndex)
	printEdfSignalHeaders(edfSignalHeaders)
	return edfHeaders{standardHeaders: edfStandardFields, signalHeaders: edfSignalHeaders}
}
